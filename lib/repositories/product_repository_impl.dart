import 'dart:async';
//import 'dart:convert';
import 'package:product_control/entities/product.dart';
import 'package:product_control/repositories/product_repository.dart';

class ProductRepositoryImpl implements ProductRepository{
  @override
  Future<List<Product>> fetchProduct() async {


    List<Product> productList = [];

    productList.add(Product(1,"Tecnologia"));
    productList.add(Product(2,"Investigacion"));
    productList.add(Product(3,"Electronica"));
    productList.add(Product(4,"Ingenieria"));

    return new Future.value(productList) ;
  }

}